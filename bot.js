const discord = require("discord.js");
const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");
const path = require("path");

const client = new discord.Client();
const server = express();

const config = JSON.parse(fs.readFileSync(path.join(__dirname, "config.json")));

let channel = null;

function sendMessage(message) {
    if (channel !== null) {
        channel.send(message);
    }
}

// http server setup
server.use(bodyParser.urlencoded({extended: false}));

server.post("/", (req, res) => {
    let message = req.body.message;
    if (message) {
        sendMessage(message);
        res.status(200).end();
    } else {
        res.status(400).end();
    }
});

server.listen(config.port);

// discord client setup
client.on("ready", () => {
    console.log("Bot is ready and listening at port " + config.port);
    channel = client.channels.get(config.channel);
});

client.login(config.token);
